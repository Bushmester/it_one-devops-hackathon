local p = import '../params.libsonnet';
local params = p.components.helm;
local renderedChart = (importstr 'data://helm');

std.native('parseYaml')(renderedChart)
